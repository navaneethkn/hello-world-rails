Rails.application.routes.draw do
  get '/authors/send_email', to: 'authors#send_email'
  resources :authors, only: [:index, :create] do
    resources :posts, only: [:index]
  end
  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home_page#main' # shortcut for the above

  require 'sidekiq/web'
mount Sidekiq::Web => '/sidekiq'
end
