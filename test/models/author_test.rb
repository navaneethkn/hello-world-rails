require 'test_helper'

class AuthorTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "author should have many posts" do
    author = Author.create!(name: "test", ph: "12")
    author.posts.create!(title: "adasd", body: "adasd", is_published: true)

    author.reload

    assert_equal 1, author.posts.count
    assert_equal "adasd", author.posts.first.title
    end
end
