require 'test_helper'

class PostTest < ActiveSupport::TestCase
  test "saving post" do
    p = Post.new
    p.title = "test post"
    p.body = "test"
    p.save!

    p = Post.find_by_title("test post")

    assert_equal p.title, "test post"
    assert_equal false, p.is_published
  end

  test "publishing the post" do
    p = Post.new(title: "Post title", is_published: true, body: "test")
    p.save!
    p.reload

    assert_equal true, p.is_published
  end

  test 'title should be set' do
    p = Post.new(title: nil, body: 'test body')
    is_valid = p.valid?
    assert_equal false, is_valid
    assert_equal p.errors.messages[:title], ["can't be blank", "is too short (minimum is 3 characters)"]
  end

  test "body should be set" do
    p = Post.new(title: "test title", body: nil)
    is_valid = p.valid?
    assert_equal false, is_valid
    assert_equal p.errors.messages[:body], ["can't be blank"]
  end

  test "title should be atleast 3 characters long" do
    p = Post.new(title: "t", body: "test body")
    is_valid = p.valid?
    assert_equal false, is_valid
    assert_equal p.errors.messages[:title], ["is too short (minimum is 3 characters)"]
  end

  test "title should get validated only when publishing" do
    p = Post.new(title: nil, body: "test")
    assert_equal true, p.valid?

    p = Post.new(title: nil, body: "test", is_published: true)
    assert_equal false, p.valid?
    assert_equal p.errors.messages[:title], ["can't be blank'"]
  end
end
