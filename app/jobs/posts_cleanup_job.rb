class PostsCleanupJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Post.where(is_published: false).destroy_all
  end
end
