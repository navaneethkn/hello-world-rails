class Post < ApplicationRecord
    belongs_to :author
    before_save :set_is_published
    validates :title, presence: true, length: { minimum: 3 }, if: :publishing?
    validates :body, presence: true
    after_create :send_email

    def set_is_published
        self.is_published = false if self.is_published.nil?
    end

    def publishing?
        return is_published
    end

    def send_email
        NotificationsMailer.post(self).deliver_later
    end
end
