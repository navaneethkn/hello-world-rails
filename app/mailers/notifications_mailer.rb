class NotificationsMailer < ApplicationMailer
  default from: 'admin@pennyful.com'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifications_mailer.post.subject
  #
  def post(post)
    @post = post
    mail to: "magesh@pennyful.com", subject: "A new post created!"
  end
end
