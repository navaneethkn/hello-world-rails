class AuthorsController < ApplicationController
  def index
    Rails.logger.debug "some test"
    @authors = Rails.cache.fetch("all_authors", expires_in: 10.minutes) do
      Author.all
    end
    @author = Author.new
  end

  def create
    @author = Author.new(author_params)

    respond_to do |format|
      if @author.save
        format.html { redirect_to authors_path, notice: 'Author was successfully created.' }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def send_email
    render json: {status: Author.all}, status: 50
  end

  def author_params
    params.require(:author).permit(:name, :ph)
  end
end
