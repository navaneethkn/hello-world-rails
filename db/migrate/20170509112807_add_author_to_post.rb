class AddAuthorToPost < ActiveRecord::Migration[5.1]
  def change
    change_table :posts do |t|
      t.references :author, index: true
    end
  end
end
